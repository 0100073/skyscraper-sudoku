# Skyscraper Sudoku Solver #

The Skyscraper Sudoku Solver is a program, written in Prolog using Constraint Logic Programming over Finite Domains (CLPFD), that solves skyscraper sudoku puzzles.

### Skycraper Sudoku ###

Skyscraper sudoku is a logic puzzle that combines the rules of skyscrapers (http://www.conceptispuzzles.com/index.aspx?uri=puzzle/skyscrapers/rules) and sudoku (http://www.conceptispuzzles.com/index.aspx?uri=puzzle/sudoku/rules). You are given a sudoku style grid and hints that either tell you which numbers go in some cells, as in sudoku, or how many elements of a given row or column are visible from a given direction, as in skyscrapers. A solution to the puzzle consists of a way to fill in all of the cells in the grid such that no row, column, or subgrid contains repeated elements, and all of the visibility constraints are satisfied.

### Running the Program ###

The repository contains two versions of the skyscraper sudoku solver. skyscraperSICStus.pl is designed to be run using SICStus Prolog and uses predicates from the SICStus lists and clpfd libraries, while skyscraperSWI is designed to be run using SWI Prolog and uses SWI's lists and clpfd libraries.

The predicate *skysudoku(problem, solution)* is used to find solutions to skysudoku problems. The problem should be given as *ss(Subgrid_Size, Constraints)*, where *Constraints* is a list of clues. Clues may be of the form *g(Number, Row, Column)*, which indicates that the given number must be at the intersection of row *Row* and column *Column*, or of the form *v(Num_Visible, Direction, Row_or_Column)*, which indicates that there must be *Num_Visible* visible numbers from one of the four directions *n*, *e*, *s*, or *w*, in the row or column numbered *Row_or_Column* (whether it is a row or column depends on the direction: *n* and *s* describe columns, while *e* and *w* describe rows).