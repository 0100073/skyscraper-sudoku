:- use_module(library(clpfd)).
:- use_module(library(lists)).
%:- use_module(library(samsort)).

% skysudoku(+SP, ?SS): SS is a solution of the Skyscraper Sudoku puzzle SP.
skysudoku(ss(N, C), SS):-
    Nsquared #= N*N,
    length(SS, Nsquared),
    row_constraints(ss(N, C), 1, 1, SS),
    transpose(SS, SST),
    row_constraints(ss(N,C), 1, 0, SST),
    subgrid_constraints(SS,N,0,0),
    labelGrid(SS).


% Applies labeling to each element of a given list.
labelGrid([]).
labelGrid([H|T]):-
    labeling([],H),
    labelGrid(T).


% row_constraints(+Problem, +Row_Num, +Row_Bool, +Solution): No rows of 
% the skyscraper sukodku grid Solution contradict any of the constraints
% in the skyscraper sudoku problem Problem that apply to any row or
% column greater than or equal to Row_Num, depending on whether 
% Row_Bool is one or zero, respectively.
row_constraints(_Problem, _Row_Num, _Row_Bool, []).
row_constraints(ss(N, C), Row_Num, Row_Bool, [Current_Row|T]):-
    Nsquared #= N*N,
    length(Current_Row,Nsquared),
    Current_Row ins 1..Nsquared,
    ADR #= 1,
    alldifferent_reif(Current_Row, ADR),
    relevant_constraints(C, Row_Num, Row_Bool, [], Rel_C),
    row_visibility_constraints(Rel_C, Row_Num, Row_Bool, Current_Row),
    Row_Num_plus_one #= Row_Num + 1,
    row_constraints(ss(N,C), Row_Num_plus_one, Row_Bool, T).


% direction_to_int(+Direction, -Int): Int is the integer between 1 and 4
% associated with Direction, which must be either n, e, s, or w.
direction_to_int(n, 1).
direction_to_int(e, 2).
direction_to_int(s, 3).
direction_to_int(w, 4).


% relevant_constraints(+Constraint_List, +Row_Num, +Row_Bool, +Solution_In_Progress, -Relevant_Subset):
% Relevant_Subset is the subset of Constraint_List that contains constraints
% that apply to rows or columns, depending on
% whether Row_Bool is 0 or 1, respectively, and Solution_In_Progress is
% a list of all of the constrains that apply to the rows or columns that
% come before Row_Num

% Case: Base Case
relevant_constraints([], _R, _B, S, S).
% Case: Given number in this row
relevant_constraints([g(N, R, C)|T], Row_Num, Row_Bool, SP, Subset):-
    (R #= Row_Num #/\ Row_Bool #= 1) #<==> Add_Bool,
    add_Constraint(g(N, R, C), Add_Bool, SP, SP2),
    relevant_constraints(T, Row_Num, Row_Bool, SP2, Subset).

relevant_constraints([v(V, D, RC)|T], Row_Num, Row_Bool, SP, Subset):-
    direction_to_int(D, Dint),
    %write(Row_Bool),
    (Dint #= 4 #/\ RC #= Row_Num #/\ Row_Bool #= 1)#\/ (Dint #= 2 #/\ RC #= Row_Num #/\ Row_Bool #= 1)#\/ (Dint #= 3 #/\ RC #= Row_Num #/\ Row_Bool #= 0)#\/ (Dint #= 1 #/\ RC #= Row_Num #/\ Row_Bool #= 0) #<==> Add_Bool,
    add_Constraint(v(V, D, RC), Add_Bool, SP, SP2),
    relevant_constraints(T, Row_Num, Row_Bool, SP2, Subset).


% add_Constraint(+Constraint, +Add_Bool, +Param_List, -Return_List):
% Return_List is Param_List if Add_Bool is 0 and is the concatenation
% of Param_List and [Constraint] if Add_Bool is 1.
add_Constraint(Constraint, 1, SP, SP2):-
    SP2 = [Constraint|SP].
add_Constraint(_Constraint, 0, SP, SP).


% row_visibility_constraints(+Constraints, +Row_Num, +Row_Bool, +R):
% R is a the Row_Numth row or column and satisfisfies the Constraints.

% Case: No constraints
row_visibility_constraints([], _Row_Num, _Row_Bool, _R).
% Case: Given value in given row
row_visibility_constraints([g(N, Row_Num, C)|T], Row_Num, 1, R):-
    C_minus_one #= C-1,
    length(Pref,C_minus_one),
    length(R, RL),
    Post_length #= RL - C,
    length(Post, Post_length),
    append(Pref,[N],Pref_Append),
    append(Pref_Append,Post,R),
    row_visibility_constraints(T, Row_Num, 1, R).

% Case: Visibility in current row from the west
row_visibility_constraints([v(V, w, Row_Num)|T], Row_Num, 1, R):-
    visnum(V, R),
    row_visibility_constraints(T, Row_Num, 1, R).
% Case: Visibility in current row from the north
row_visibility_constraints([v(V, n, Row_Num)|T], Row_Num, 0, R):-
    visnum(V, R),
    row_visibility_constraints(T, Row_Num, 0, R).
% Case: Visibility in current row from the east
row_visibility_constraints([v(V, e, Row_Num)|T], Row_Num, 1, R):-
    reverse(R, R_rev),
    visnum(V, R_rev),
    row_visibility_constraints(T, Row_Num, 1, R).
% Case: Visibility in current row from the south
row_visibility_constraints([v(V, s, Row_Num)|T], Row_Num, 0, R):-
    reverse(R, R_rev),
    visnum(V, R_rev),
    row_visibility_constraints(T, Row_Num, 0, R).
 

% subgrid_constraints(+SS, +N, X_coordinate, Y_coordinate):
% All of the subgrids that, in the grid of NxN subgrids, have a
% x coordinate greater than or equal to X_coordinate and a y 
% coordinate greater than or equal to Y_coordinate contain no
% repeated elements.
subgrid_constraints(_SS, _N, _N, _Y_co).
subgrid_constraints(SS, N, X_co, Y_co):-
    Nsquared #= N*N,
    X_co #< N,
    Y_co #< N,
    subgrid(SS, N, X_co, Y_co, 0, [], Subgrid0),
    length(Subgrid0, Nsquared),
    ADR #= 1,
    alldifferent_reif(Subgrid0, ADR),
    Nsub1 #= N - 1,
    Y_co #< Nsub1 #<=> Y_below_lim,
    Y_co #= Nsub1 #<=> Y_at_lim,
    Y_co_n #= (Y_co + 1) * Y_below_lim,
    X_co_n #= X_co + Y_at_lim,
    subgrid_constraints(SS, N, X_co_n, Y_co_n).


% subgrid(+Sudoku_Grid, +SGLength, +XCoord, +YCoord, +Rowstep0, +WIPList0, +Subgrid): 
% Subgrid is the subgrid of Sudoku_Grid located at the intersection of
% the XCoord-th column and YCoord-th row of grid of subgrids of Sudoku_Grid.

subgrid(_Grid, SGLength, _XCoord, _YCoord, SGLength, WIPList0, WIPList0).

subgrid(Grid, SGLength, XCoord, YCoord, Rowstep0, WIPList0, Subgrid):-
    NSquared #= SGLength * SGLength,
    length_check(Grid, NSquared),
    Rows_before #= Rowstep0 + YCoord * SGLength,
    sublist(Grid, [Row0], Rows_before, 1, _After0), % Row0 is the Row_to_get-th row
    Cols_before #= XCoord * SGLength,
    sublist(Row0, RBI, Cols_before, SGLength, _After1),
    append(RBI, WIPList0, WIPList1),
    Rowstep1 #= Rowstep0 + 1,
    subgrid(Grid, SGLength, XCoord, YCoord, Rowstep1, WIPList1, Subgrid).


% sublist(+Whole, ?Part, ?Before, ?Length, ?After): Part is a 
% sublist of the Whole such that there are Before number 
% of elements in Wole before Part, After number of elements in
% Whole after Part, and the length of Part is Length
sublist(Whole, Part, Before, Length, After) :-
    append(Section1, Section2, Whole),
    length(Section1, Before),
    append(Part, Section3, Section2),
    length(Section3, After),
    length(Part, Length).


% length_check(+List, -Length): All elements of List are lists of length Length.
length_check([], _X).
length_check([H|T], Length):-
    length(H, Length),
    length_check(T, Length).

% alldifferent_reif(List, Bool): (Using reification) Bool is 1 if
% all of the elements of List are distinct and 0 otherwise
alldifferent_reif(L, 1):-
    all_distinct(L).
alldifferent_reif(L,0):-
    repetition(L).

% repetition(+List): List contains at least one repeated element
repetition([A,B|T]):-
    A #= B;
    repetition([A|T]);
    repetition([B|T]).


% visnum(?K, ?L): THe number of elements in list L that are visible from the left is K.
visnum(K, L):-
    visnum(L, 0, 0, K).
visnum([], _M, K, K).
visnum([H|T], M, NumVis, K):-
    H #> M #<==> HGM,
    NewNumVis #= NumVis + HGM,
    NewMax #= max(M,H),
    visnum(T, NewMax, NewNumVis, K).